﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullterRight : MonoBehaviour
{
    Rigidbody2D rb2d;
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        Invoke("destroyBullet", .5f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb2d.velocity = new Vector2(20, 0);
    }
    void destroyBullet()
    {
        Destroy(gameObject);
    }
}
