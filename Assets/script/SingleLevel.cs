﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SingleLevel : MonoBehaviour
{
    private int CurrentStarNum = 0;
    public int Level_Index;
    public static SingleLevel instance;
    private void Awake()
    {
        if (instance != null)
            return;
        else
            instance = this;
        
    }

    private void Start()
    {
       
    }
  
    public void loadSceneMenu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void PressStarButton(int _starNum)
    {
        CurrentStarNum = _starNum;
        if (CurrentStarNum > PlayerPrefs.GetInt("Lv" + Level_Index))
        {
            PlayerPrefs.SetInt("Lv" + Level_Index, _starNum);
        }
        
    }

}
