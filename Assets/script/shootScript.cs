﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class shootScript : MonoBehaviour
{
    public Camera cameraEditor;
    public Transform shotPoint;
    public GameObject Bullet;
    public float ButtletSpeed;
    public GameObject gunSelected;
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 origin = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                                        Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
            RaycastHit2D hit = Physics2D.Raycast(GetCurrentPlatformClickPosition(cameraEditor), Vector2.zero);
            gunSelected = hit.transform.gameObject;
            if (hit.collider != null)

            {
                if (hit.collider.CompareTag("gun"))
                {
                    if (gunSelected.gameObject == this.gameObject)
                    {
                        GameObject buttletIns = Instantiate(Bullet, shotPoint.position, shotPoint.rotation);
                    }
                   
                }
            }
        }
    }
    void ShootRight()
    {
        GameObject buttletIns = Instantiate(Bullet, shotPoint.position, shotPoint.rotation);
       buttletIns.GetComponent<Rigidbody2D>().AddForce(buttletIns.transform.right);
    }   
    private Vector3 GetCurrentPlatformClickPosition(Camera camera)
    {
        Vector3 clickPosition = Vector3.zero;

        if (Application.isMobilePlatform)
        {//current platform is mobile
            if (Input.touchCount != 0)
            {
                Touch touch = Input.GetTouch(0);
                clickPosition = touch.position;
            }
        }
        else
        {//others
            clickPosition = Input.mousePosition;
        }
        clickPosition = camera.ScreenToWorldPoint(clickPosition);//get click position in the world space
        clickPosition.z = 0;
        return clickPosition;

    } 
}
