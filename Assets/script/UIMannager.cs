﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMannager : MonoBehaviour
{
    public Text allStar;

    // Update is called once per frame
    void Update()
    {
        updateStar();
    }
    private void updateStar()
    {
        int Sum = 0;
        for (int i = 0; i < 5; i++)
        {
            Sum += PlayerPrefs.GetInt("Lv" + i.ToString());
            allStar.text = Sum + "/" + 15;
        }
    }
}
