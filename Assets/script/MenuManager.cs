﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MenuManager : MonoBehaviour
{
  
    public int HightScore;
   // public Text AllStar;
    public GameObject Menu;
    public GameObject PlayMenu;
    public static MenuManager instance;
    private void Awake()
    {
        if (instance != null)
            return;
        else

            instance = this;
    }
    void Start()
    {
        PlayMenu.SetActive(false);
       /* setCoutext();*/
    }
    public void ButtonPlay()
    {
        Menu.SetActive(false);
        PlayMenu.SetActive(true);
    }
   
    public void loadScene1()
    {
        SceneManager.LoadScene("lv1");
    }
    public void loadScene2()
    {
        SceneManager.LoadScene("lv2");
    }
    public void loadScene3()
    {
        SceneManager.LoadScene("lv3");
    }
    // Update is called once per frame
  /*  void setCoutext() 
    {
      //  HightScore = Weight.instance.allStar;

        AllStar.text = HightScore.ToString();
    }*/
}
