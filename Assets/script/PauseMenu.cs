﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static bool IsPause;
    public GameObject PauseMenuu;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (IsPause)
            {
                resume();
            }else
            {
                Pause();
            }
        }
       
    }
    public void resume() 
    {
        PauseMenuu.SetActive(false);
        Time.timeScale = 1f;
        IsPause = false;
    }
   public void Pause()
    {
        PauseMenuu.SetActive(true);
        Time.timeScale = 0f;
        IsPause = true;
    }
}
