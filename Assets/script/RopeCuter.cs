﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeCuter : MonoBehaviour
{
    public Camera cameraEditor;
    public static Rope rope;
    void Start()
    {
        cameraEditor = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 origin = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                                        Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
            RaycastHit2D hit = Physics2D.Raycast(GetCurrentPlatformClickPosition(cameraEditor), Vector2.zero);
            
            if (hit.collider != null)
            {
                
                if (hit.collider.CompareTag("link"))
                {
                
                   Destroy(hit.transform.parent.gameObject);
                   // Destroy(hit.collider.GetComponent<Rope>().linkPrefab.gameObject);
                    
                }
            }
        }
    }
    private Vector3 GetCurrentPlatformClickPosition(Camera camera)
    {
        Vector3 clickPosition = Vector3.zero;

        if (Application.isMobilePlatform)
        {//current platform is mobile
            if (Input.touchCount != 0)
            {
                Touch touch = Input.GetTouch(0);
                clickPosition = touch.position;
            }
        }
        else
        {//others
            clickPosition = Input.mousePosition;
        }

        clickPosition = camera.ScreenToWorldPoint(clickPosition);//get click position in the world space
        clickPosition.z = 0;
        return clickPosition;
    }
}
