﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Weight : MonoBehaviour
{
    public GameObject gameOver;
    public GameObject[] congrat;
    public int score;
    public float disStanceChainEnd = 0.6f;
    private Rigidbody2D rb2d;
    public static Weight instance;
    public int StatusStar;
    

    public GameObject[] star; //1

    private void Awake()
    {
        if (instance != null)
            return;
        else

            instance = this;
    }
    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        gameOver.SetActive(false);
        score = 0;
       
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "coin")
        {
            Destroy(collision.gameObject);
            score = score + 1;
            StatusStar = score;
        }
        if(collision.gameObject.tag == "floor")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (collision.gameObject.tag == "frog")
        {            
            Destroy(gameObject);
            gameOver.SetActive(true);          
            starAcheived();        
            SingleLevel.instance.PressStarButton(score);
          
        }
        
    }
    public void ConnectRopeEnd(Rigidbody2D endRB)
    {
        HingeJoint2D joint = gameObject.AddComponent<HingeJoint2D>();
        joint.autoConfigureConnectedAnchor = false;
        joint.connectedBody = endRB;
        joint.anchor = Vector2.zero;
        joint.connectedAnchor = new Vector2(0f,-disStanceChainEnd);
    }
    public void starAcheived()// complete star
    {
        
        
        float percentage = float.Parse(score.ToString()) / 3 * 100;
        if (percentage < 34&& percentage > 0)
        {
            star[0].SetActive(true);
            congrat[0].SetActive(true);
        }
        else if (percentage > 35f && percentage < 67)
        {
            star[0].SetActive(true);
            star[1].SetActive(true);
            congrat[1].SetActive(true);
        }
        else if(percentage==100)
        {
            {
                star[0].SetActive(true);
                star[1].SetActive(true);
                star[2].SetActive(true);
                congrat[2].SetActive(true);
            }
        }
        else
        {
            congrat[3].SetActive(true);
        }
        

    }
}
