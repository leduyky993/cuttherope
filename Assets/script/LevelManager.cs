﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public bool UnLock = false;
    // public bool LevelComplete;
    public static LevelManager instance;
    public Image UnlockImage;
    public GameObject[] star;
    public Sprite StarSprite;
    public GameObject blockLevel;
    
    private void Awake()
    {
        if (instance != null)
            return;
        else
            instance = this;
    }
    
    private void Update()
    {
        
        UpdateLevelImage();
        UpdateLevelStatus();
        
    }
    public void UpdateLevelStatus()
    {
        int previousLevelNum = int.Parse(blockLevel.name) - 1;
        if (PlayerPrefs.GetInt("Lv" + previousLevelNum.ToString()) > 0)
        {
            UnLock = true;
        }
     
    }
    public void UpdateLevelImage ()
    {
       
        if (!UnLock)
        {
            UnlockImage.gameObject.SetActive(true);
            for (int i = 0; i < star.Length; i++)
            {
                star[i].gameObject.SetActive(false);
            }
        }
        else
        {
            UnlockImage.gameObject.SetActive(false);
            for (int i = 0; i < star.Length; i++)
            {
                star[i].gameObject.SetActive(true);
            }
            for(int i = 0; i <PlayerPrefs.GetInt("Lv" + gameObject.name); i++)
            {
                star[i].gameObject.GetComponent<Image>().sprite = StarSprite;
            }
        }
    }
   
    public void SelectionLevel(string _NameLevel)
    {
      
        if (UnLock)
        {
            SceneManager.LoadScene(_NameLevel);
        }
    }
}
